# People of MIT #

"People of MIT" is an Anroid app, which shows informations about students and professors of  Massachusetts Institute of Technology.
Note: This app handles fake data, it's made for presentation purpose only.


### Main functions: ###

* connect with API ([https://randomuser.me](https://randomuser.me))
* fetch data (JSON format) of students and professors (name, gender, date of birth, phone, mail)
* add extra user data of students/professors for presentation purpose (department, year of study, years of work, academic title)
* display students/professors on a list
* show details of person when it is selected on a list
* search on a list : query with gender and department filter 
