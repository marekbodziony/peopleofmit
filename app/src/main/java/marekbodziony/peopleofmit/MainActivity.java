package marekbodziony.peopleofmit;

import android.content.Intent;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import marekbodziony.peopleofmit.api.Library;
import marekbodziony.peopleofmit.api.PersonsAdapter;
import marekbodziony.peopleofmit.model.Person;


public class MainActivity extends AppCompatActivity implements PersonsAdapter.PersonItemClickListener{

    private static final int SEARCH_REQUEST_CODE = 1;

    private String personType;

    private FragmentManager fragmentManager;
    private PersonsListFragment listFragment;
    private PersonDetailsFragment personFragment;

    private boolean backToListFragment = false;      // if list fragment should be shown when back btn is pressed in details screen

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        personType = getIntent().getStringExtra(Library.INTENT_DATA_TYPE);
        setTitle(personType);

        listFragment = new PersonsListFragment();
        listFragment.setPersonType(personType);
        listFragment.setClickListener(this);
        personFragment = new PersonDetailsFragment();

        fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().add(R.id.persons_list_fragment_container, listFragment).commit(); // display persons list fragment
    }

    // when item on a list is clicked show details of this person
    @Override
    public void onPersonItemClick(Person p) {
        Log.d(Library.TAG,"Person clicked = " + p.getName());
        getSupportActionBar().hide();
        backToListFragment = true;
        personFragment.setPerson(p);
        fragmentManager.beginTransaction().replace(R.id.persons_list_fragment_container, personFragment).commit(); // display person details fragment
    }

    @Override
    public void onBackPressed() {
        // list fragment should be shown when back btn is pressed in details screen
        if (backToListFragment) {
            getSupportActionBar().show();
            fragmentManager.beginTransaction().replace(R.id.persons_list_fragment_container, listFragment).commit();
            backToListFragment = false;
        }else{
            super.onBackPressed();
        }
    }

    // create menu with one item - Search
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // when user click on 'search' menu icon
        if (item.getItemId() == R.id.search){
            Intent intent = new Intent(this, SearchActivity.class);
            intent.putExtra(Library.INTENT_DATA_TYPE,personType);
            startActivityForResult(intent, SEARCH_REQUEST_CODE);
            return  true;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // get search query and pass it to PersonsListFragment
        if (requestCode == SEARCH_REQUEST_CODE && resultCode == RESULT_OK){
            String query = data.getStringExtra(Library.INTENT_SEARCH_QUERY);
            Boolean female = data.getBooleanExtra(Library.INTENT_SEARCH_FEMALE,false);
            Boolean male = data.getBooleanExtra(Library.INTENT_SEARCH_MALE,false);
            String dep = data.getStringExtra(Library.INTENT_SEARCH_DEPARTMENT);
            listFragment.setSearchValues(query,female,male,dep);
        }
    }
}
