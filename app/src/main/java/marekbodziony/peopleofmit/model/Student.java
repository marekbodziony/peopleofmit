package marekbodziony.peopleofmit.model;


public class Student extends Person {

    private int yearOfStudy;    // no available in API, will be set manually (for presentation purpose)

    // constructor
    public Student(String name, String gender, String phone, String mail, String dateOfBirth, String tumbnail, String image){
        super(name, gender, phone, mail, dateOfBirth, tumbnail, image);
    }

    // getters
    public int getYearOfStudy() {return yearOfStudy;}


    // set fake year of study
    // this data is not available in API, it's set for presentation purpose only
    public void setFakeYearOfStudy(){
        int id = this.getId();
        if (id%5 == 0) yearOfStudy = 5;
        else if (id%4 == 0) yearOfStudy = 4;
        else if (id%3 == 0) yearOfStudy = 3;
        else if (id%2 == 0) yearOfStudy = 2;
        else yearOfStudy = 1;
    }
}
