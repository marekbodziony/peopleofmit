package marekbodziony.peopleofmit.model;

import marekbodziony.peopleofmit.R;
import marekbodziony.peopleofmit.api.Library;

public class Department {

    private static String department;

    // assign student to a group
    // data not available in DB, for presentation purpose only
    public static String assign(int id){

        if (id%50 == 0) department = Library.DEP_AERONAUTICS_AND_ASTRONAUTICS;
        else if (id%25 == 0) department = Library.DEP_ARCHITECTURE;
        else if (id%12 == 0) department = Library.DEP_BIOLOGY;
        else if (id%11 == 0) department = Library.DEP_CHEMISTRY;
        else if (id%10 == 0) department = Library.DEP_CIVIL_AND_ENVIRONMENTAL_ENGINEERING;
        else if (id%7 == 0) department = Library.DEP_ECONOMICS;
        else if (id%6 == 0) department = Library.DEP_ELECTRICAL_ENGINEERING_AND_COMPUTER_SCIENCE;
        else if (id%5 == 0) department = Library.DEP_LITERATURE;
        else if (id%4 == 0) department = Library.DEP_MATHEMATICS;
        else if (id%3 == 0) department = Library.DEP_NUCLEAR_SCIENCE_AND_ENGINEERING;
        else department = Library.DEP_PHYSICS;

        return department;
    }
}
