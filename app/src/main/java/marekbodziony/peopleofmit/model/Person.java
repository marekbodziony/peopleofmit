package marekbodziony.peopleofmit.model;


public class Person {

    private int id;                     // no available in API, will be set manually (for presentation purpose)
    private String name;
    private String gender;
    private String phone;
    private String email;
    private String dateOfBirth;
    private String department;          // no available in API, will be set manually (for presentation purpose)
    private String thumbnailURL;
    private String imageURL;
    private boolean favourite;



    // constructor
    public Person(String name, String gender, String phone, String email, String dateOfBirth, String thumbnailURL, String imageURL){
        this.name = name;
        this.gender = gender;
        this.phone = phone;
        this.email = email;
        department = "";
        this.dateOfBirth = dateOfBirth;
        this.thumbnailURL = thumbnailURL;
        this.imageURL = imageURL;
    }

    // getters
    public int getId() {return id;}
    public String getName() {return name;}
    public String getGender() {return gender;}
    public String getPhone() {return phone;}
    public String getEmail() {return email;}
    public String getDateOfBirth() {return dateOfBirth;}
    public String getDepartment() {return department;}
    public String getThumbnailURL() {return thumbnailURL;}
    public String getImageURL() {return imageURL;}
    public boolean getFavourite() {return favourite;}

    // setters
    public void setId(int id) {this.id = id;}
    public void setFavourite(boolean favourite) {this.favourite = favourite;}

    // assign student to a group (for presentation purpose only)
    public void assignToDepartment() {department = Department.assign(id);}
}
