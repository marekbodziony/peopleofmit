package marekbodziony.peopleofmit.model;


public class Professor extends Person{

    private String title = "";      // no available in API, will be set manually (for presentation purpose)
    private int yearsOfWork;        // no available in API, will be set manually (for presentation purpose)

    // constructor
    public Professor(String name, String gender , String phone, String mail, String dateOfBirth, String tumbnail, String image) {
        super(name, gender, phone, mail, dateOfBirth, tumbnail, image);
    }

    // getter
    public String getTitle() {return title;}
    public int getYearsOfWork() {return yearsOfWork;}



    //  set fake years of work
    // this data is not available in API, it's set for presentation purpose only
    public void setFakeYearsOfWork(){
        int id = this.getId();
        if (id%50 == 0) yearsOfWork = 45;
        else if (id%10 == 0) yearsOfWork = 32;
        else if (id%7 == 0) yearsOfWork = 28;
        else if (id%5 == 0) yearsOfWork = 21;
        else if (id%4 == 0) yearsOfWork = 18;
        else if (id%3 == 0) yearsOfWork = 13;
        else if (id%2 == 0) yearsOfWork = 7;
        else yearsOfWork = 5;
    }
    // set fake academic title
    // this data is not available in API, it's set for presentation purpose only
    public void setFakeAcademicTitle(){
        String [] titles = {"PhD","MSc","EdD"};
        int id = this.getId();
        if (id%5 == 0) title = titles[0];
        else if (id%2 == 0) title = titles[1];
        else title = titles[2];;
    }
}
