package marekbodziony.peopleofmit;

import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import marekbodziony.peopleofmit.api.Library;
import marekbodziony.peopleofmit.api.NetworkUtils;
import marekbodziony.peopleofmit.api.PersonsAdapter;
import marekbodziony.peopleofmit.model.Department;
import marekbodziony.peopleofmit.model.Person;


public class PersonsListFragment extends Fragment implements PersonsAdapter.PersonItemClickListener{

    private List<Person> personsList;
    private String personType;

    private List<Person> searchList;        // holds query results

    private PersonsAdapter adapter;
    private RecyclerView recyclerView;
    private ProgressBar progressBar;        // show progress bar while data is uploaded in background thread
    private TextView errorTextView;         // displays error message when there is no Internet connection

    private static PersonsAdapter.PersonItemClickListener clickListener;

    private String searchQuery;
    private boolean searchFemale;
    private boolean searchMale;
    private String searchDep;

    // constructor
    public PersonsListFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_persons_list,container,false);

        progressBar = (ProgressBar) view.findViewById(R.id.fragment_persons_list_progressBar);
        errorTextView = (TextView) view.findViewById(R.id.fragment_persons_list_error_textView);

        personsList = new ArrayList<>();
        getPersonsFromHttp(personType);

        adapter = new PersonsAdapter(getContext(), personsList, personType,this);
        recyclerView = (RecyclerView)view.findViewById(R.id.fragment_persons_list_recyclerView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(adapter);

//        searchList = new ArrayList<>();

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        // when back from search activity with query values do search
        if (searchQuery != null) {
            searchList = queryPersonsList(searchQuery, searchFemale,searchMale,searchDep);
            adapter = new PersonsAdapter(getContext(),searchList,personType,this);
            recyclerView.setAdapter(adapter);
            searchQuery = null;
        }
    }

    @Override
    public void onPersonItemClick(Person p) {
        clickListener.onPersonItemClick(p);
//        Log.d(Library.TAG,personType + " clicked = " + personsList.get(position).getName());
    }

    // setters
    public void setPersonType(String type){personType = type;}
    public void setClickListener (PersonsAdapter.PersonItemClickListener clickListener){
        this.clickListener = clickListener;
    }
    public void setSearchValues(String query, boolean female, boolean male, String dep) {
        searchQuery = query;
        searchFemale = female;
        searchMale = male;
        searchDep = dep;
    }


    // start background thread and get data from http response
    private void getPersonsFromHttp(String type){
        URL url = null;
        try {
            if (type.equals(Library.STUDENT)) url = new URL(Library.STUDENTS_URL);
            else if (type.equals(Library.PROFESSOR)) url = new URL(Library.PROFESSORS_URL);

            new GetDataFromUrlAsyncTask().execute(url); // start background thread
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    // AsyncTask used to get data from http response in background thread
    public class GetDataFromUrlAsyncTask extends AsyncTask<URL, Void, List<Person>>{
        @Override
        protected void onPreExecute() {
            progressBar.setVisibility(View.VISIBLE);
//            Log.d(Library.TAG,"onPreExecute() \t| show progress bar");
        }

        @Override
        protected List<Person> doInBackground(URL... urls) {
            URL url = urls[0];
            List<Person> list = new ArrayList<>();
            try {
//                Log.d(Library.TAG,"doInBackground()\t| start new thread, get data from Internet");
                list = NetworkUtils.getPersonsListFromUrl(personType, url);
            } catch (IOException e) {
                e.printStackTrace();
                Log.d(Library.TAG,"Error! Problem with Internet connection");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return list;
        }

        @Override
        protected void onPostExecute(List<Person> persons) {
            progressBar.setVisibility(View.GONE);
            personsList.clear();
            personsList.addAll(persons);
            adapter.notifyDataSetChanged();
            if (personsList == null || personsList.size() == 0) errorTextView.setVisibility(View.VISIBLE);
//            Log.d(LOG_TAG,"onPostExecute() \t| hide progress bar, display data or error message");
            Log.d(Library.TAG, personType + " list size = " + personsList.size());
        }
    }

    // method searching in persons list for query phrase with specified conditions (gender, department)
    private List<Person> queryPersonsList(String query, boolean female, boolean male, String dep){

        List<Person> result = new ArrayList<>();

        if (!female && !male) return null;   // if no gender selected finish searching with no results
        for(Person p : personsList){
            if (dep.equals(Library.DEP_SEARCH_IN_ALL_DEPARTMENTS)){
                if(female && !male){
                    if (p.getGender().equals(Library.INTENT_SEARCH_FEMALE) && p.getName().toLowerCase().contains(query.toLowerCase())) result.add(p);
                }
                else if (!female && male){
                    if (p.getGender().equals(Library.INTENT_SEARCH_MALE) && p.getName().toLowerCase().contains(query.toLowerCase())) result.add(p);
                }
                else {
                    if (p.getName().toLowerCase().contains(query.toLowerCase())) result.add(p);
                }
            }else{
                if(female && !male){
                    if (p.getDepartment().equals(dep) &&  p.getGender().equals(Library.INTENT_SEARCH_FEMALE) && p.getName().toLowerCase().contains(query.toLowerCase())) result.add(p);
                }
                else if (!female && male){
                    if (p.getDepartment().equals(dep) &&  p.getGender().equals(Library.INTENT_SEARCH_MALE) && p.getName().toLowerCase().contains(query.toLowerCase())) result.add(p);
                }
                else {
                    if (p.getDepartment().equals(dep) &&  p.getName().toLowerCase().contains(query.toLowerCase())) result.add(p);
                }
            }
        }
        if (result.size() == 0) Toast.makeText(getContext(),"No results",Toast.LENGTH_LONG).show();
        // show searching results in log - for debugging
        Log.d(Library.TAG,"Search: query=" + searchQuery + ", dep=" + searchDep + ". Person list size=" + personsList.size());
        Log.d(Library.TAG,"Results : " + result.size());
        Person p;
        for (int i = 0; i < result.size(); i++){
            p = (result.get(i));
            Log.d(Library.TAG,(i+1) + ". " + p.getName() + " (pos=" + p.getId() + ", dep=" + p.getDepartment() + ")");
        }
        return result;
    }

}
