package marekbodziony.peopleofmit.api;


/**
 *  This Library class is holding all statics Strings which are used in application
 */

public class Library {

    public static final String TAG = "LogMIT";

    public static final String STUDENTS_URL = "https://randomuser.me/api/?noinfo&inc=name,gender,dob,email,phone,picture&results=500&seed=students";
    public static final String PROFESSORS_URL = "https://randomuser.me/api/?noinfo&inc=name,gender,dob,email,phone,picture&results=100&seed=professors";

    public static final String STUDENT = "Student";
    public static final String PROFESSOR = "Professor";

    public static final String INTENT_DATA_TYPE = "type";
    public static final String INTENT_SEARCH_QUERY = "search";
    public static final String INTENT_SEARCH_FEMALE = "female";
    public static final String INTENT_SEARCH_MALE = "male";
    public static final String INTENT_SEARCH_DEPARTMENT = "department";


    public static final String DEP_SEARCH_IN_ALL_DEPARTMENTS = "Search in all departments";
    public static final String DEP_AERONAUTICS_AND_ASTRONAUTICS = "Aeronautics and Astronautics";
    public static final String DEP_ARCHITECTURE = "Architecture";
    public static final String DEP_BIOLOGY = "Biology";
    public static final String DEP_CHEMISTRY = "Chemistry";
    public static final String DEP_CIVIL_AND_ENVIRONMENTAL_ENGINEERING = "Civil and Environmental Engineering";
    public static final String DEP_ECONOMICS = "Economics";
    public static final String DEP_ELECTRICAL_ENGINEERING_AND_COMPUTER_SCIENCE = "Electrical Engineering and Computer Science";
    public static final String DEP_LITERATURE = "Literature";
    public static final String DEP_MATHEMATICS = "Mathematics";
    public static final String DEP_NUCLEAR_SCIENCE_AND_ENGINEERING = "Nuclear Science and Engineering";
    public static final String DEP_PHYSICS = "Physics";

}
