package marekbodziony.peopleofmit.api;

import com.bumptech.glide.annotation.GlideModule;
import com.bumptech.glide.module.AppGlideModule;


// Ensures that Glide's generated API is created.
// needed for uploading images to ImageViews

@GlideModule
public final class GlideAPI extends AppGlideModule {}
