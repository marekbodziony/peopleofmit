package marekbodziony.peopleofmit.api;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import marekbodziony.peopleofmit.model.Person;
import marekbodziony.peopleofmit.model.Professor;
import marekbodziony.peopleofmit.model.Student;

public class NetworkUtils {

    // gets http response (JSON file) from URL
    public static List<Person> getPersonsListFromUrl(String type, URL url) throws IOException, JSONException {

        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
        String json;
        List<Person> personsList = new ArrayList<>();

        try{
            InputStream in = urlConnection.getInputStream();

            Scanner scanner = new Scanner(in);
            scanner.useDelimiter("\\A");

            if (scanner.hasNext()){
                json = scanner.next();
                personsList = parseJSONToPersonsList(type,json);
            }
            in.close();
            return personsList;
        }
        finally {
            urlConnection.disconnect();
        }
    }

    // gets list of persons from JSON file
    private static List<Person> parseJSONToPersonsList(String type, String jsonString) throws IOException, JSONException {

        List<Person> personsList = new ArrayList<>();
        String name, first, last, gender, dateOfBirth, email, phone, thumbnail, image;

        JSONArray allPersonsJSON = new JSONObject(jsonString).getJSONArray("results");
        JSONObject personJSON, nameJSON, imgJSON;

        for (int i = 0; i < allPersonsJSON.length(); i++){

            Student s;
            Professor p;
            personJSON = allPersonsJSON.getJSONObject(i);
            nameJSON = personJSON.getJSONObject("name");
            imgJSON = personJSON.getJSONObject("picture");
            first = nameJSON.getString("first");
            first = first.substring(0,1).toUpperCase() + first.substring(1,first.length());
            last = nameJSON.getString("last");
            last = last.substring(0,1).toUpperCase() + last.substring(1,last.length());
            name =  first + " " + last;
            gender = personJSON.getString("gender");
            phone = personJSON.getString("phone");
            email = personJSON.getString("email");
            dateOfBirth = personJSON.getString("dob").substring(0,10);
            thumbnail = imgJSON.getString("thumbnail");
            image = imgJSON.getString("large");            // large = 128x128, medium = 72x72

            if (type.equals(Library.STUDENT)){
                s = new Student(name, gender, phone, email, dateOfBirth, thumbnail, image);
                personsList.add(s);
                s.setId(personsList.indexOf(s)+1);          // set Student id
                s.assignToDepartment();                     // assign to department (for presentation purpose)
                s.setFakeYearOfStudy();                     // set fake year of study (for presentation purpose)
            } else if (type.equals(Library.PROFESSOR)){
                p = new Professor(name, gender, phone, email, dateOfBirth, thumbnail, image);
                personsList.add(p);
                p.setId(personsList.indexOf(p)+1);          // set Professor id
                p.setFakeAcademicTitle();                   // set fake academic title (for presentation purpose)
                p.assignToDepartment();                     // assign to department (for presentation purpose)
                p.setFakeYearsOfWork();                     // set fake years of work (for presentation purpose)
            }
        }
        return personsList;
    }
}
