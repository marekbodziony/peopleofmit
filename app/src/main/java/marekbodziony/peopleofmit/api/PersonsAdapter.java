package marekbodziony.peopleofmit.api;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import java.util.List;

import marekbodziony.peopleofmit.R;
import marekbodziony.peopleofmit.model.Person;
import marekbodziony.peopleofmit.model.Professor;
import marekbodziony.peopleofmit.model.Student;


public class PersonsAdapter extends RecyclerView.Adapter<PersonsAdapter.PersonViewHolder>{

    private Context context;
    private List<Person> personsList;
    private String personType;

    private static PersonItemClickListener clickListener;

    // constructor
    public PersonsAdapter(Context context, List<Person> personsList, String personType, PersonItemClickListener clickListener){
        this.context = context;
        this.personType = personType;
        this.clickListener = clickListener;
        this.personsList = personsList;
    }

    @Override
    public PersonViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.single_item_persons_list,parent,false);
        PersonViewHolder holder = new PersonViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(PersonViewHolder holder, int position) {
        Person p = personsList.get(position);
        holder.name.setText(p.getName());
        holder.department.setText(p.getDepartment());
        if (personType.equals(Library.STUDENT)) {
            holder.yearOfStudy.setText(""+((Student)p).getYearOfStudy());       // show years of study when displaying student
        }else if (personType.equals(Library.PROFESSOR)) {
            holder.yearsTxt.setVisibility(View.INVISIBLE);                      // hide years of study when displaying professors
            holder.yearOfStudy.setVisibility(View.INVISIBLE);                   // hide years of study when displaying professors
            holder.title.setText(", " + ((Professor)p).getTitle());              // add academic title next to professor name
            holder.title.setVisibility(View.VISIBLE);
            holder.itemCard.setCardBackgroundColor(context.getResources().getColor(R.color.professorCard));     // change items color
        }
        GlideApp.with(context).load(p.getImageURL()).placeholder(R.drawable.mit_logo).error(R.drawable.no_image).into(holder.thumbnail);
    }

    @Override
    public int getItemCount() {
        return personsList.size();
    }


    // Provide a reference to the views for each data item
    public class PersonViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        CardView itemCard;
        TextView name;
        TextView title;
        TextView department;
        TextView yearOfStudy;
        TextView yearsTxt;
        ImageView thumbnail;

        public PersonViewHolder(View itemView) {
            super(itemView);
            name = (TextView)itemView.findViewById(R.id.list_person_name_textView);
            title = (TextView) itemView.findViewById(R.id.list_person_title_textView);
            department = (TextView)itemView.findViewById(R.id.list_department_textView);
            yearOfStudy = (TextView)itemView.findViewById(R.id.list_year_value_textView);
            yearsTxt = (TextView)itemView.findViewById(R.id.list_year_txt_textView);
            thumbnail = (ImageView)itemView.findViewById(R.id.list_image_imageView);
            itemCard = (CardView)itemView.findViewById(R.id.person_item_cardView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            int position = getAdapterPosition();
            clickListener.onPersonItemClick(personsList.get(position));
        }
    }

    // interface needed to communicate between master list fragment and detail fragments
    public interface PersonItemClickListener {
        void onPersonItemClick(Person p);
    }
}
