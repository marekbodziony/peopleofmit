package marekbodziony.peopleofmit;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;

import marekbodziony.peopleofmit.R;
import marekbodziony.peopleofmit.api.Library;

public class SearchActivity extends AppCompatActivity {

    private EditText queryTxt;
    private Spinner department;
    private CheckBox femaleCheckBox;
    private CheckBox maleCheckBox;
    private Button searchBtn;
    private ArrayAdapter<CharSequence> departmentAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        setTitle(getString(R.string.search));

        queryTxt = (EditText) findViewById(R.id.search_queryText);
        department = (Spinner)findViewById(R.id.serach_department_spinner);
        femaleCheckBox = (CheckBox) findViewById(R.id.search_female_check);
        maleCheckBox = (CheckBox) findViewById(R.id.search_male_check);
        searchBtn = (Button) findViewById(R.id.search_search_btn);

        departmentAdapter = ArrayAdapter.createFromResource(this,R.array.search_departments,R.layout.support_simple_spinner_dropdown_item);
        department.setAdapter(departmentAdapter);

        // when 'search' button was clicked back to previous activity (with query text if it was typed)
        searchBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (queryTxt.getText().length() > 0) {
                    Intent i = getIntent();
                    i.putExtra(Library.INTENT_SEARCH_QUERY,queryTxt.getText().toString());
                    i.putExtra(Library.INTENT_SEARCH_FEMALE,femaleCheckBox.isChecked());
                    i.putExtra(Library.INTENT_SEARCH_MALE,maleCheckBox.isChecked());
                    i.putExtra(Library.INTENT_SEARCH_DEPARTMENT,(String)department.getSelectedItem());
                    setResult(RESULT_OK,i);
                }
                finish();
            }
        });
    }
}
