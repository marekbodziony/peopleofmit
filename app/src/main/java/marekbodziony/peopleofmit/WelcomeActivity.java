package marekbodziony.peopleofmit;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

import marekbodziony.peopleofmit.api.Library;
import marekbodziony.peopleofmit.model.Person;

public class WelcomeActivity extends AppCompatActivity {



    private ImageButton studentsBtn;
    private ImageButton professorsBtn;

    private Intent personsListIntent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);
        getSupportActionBar().hide();

        personsListIntent = new Intent(this,MainActivity.class);
        studentsBtn = (ImageButton) findViewById(R.id.students_btn);
        professorsBtn = (ImageButton) findViewById(R.id.professors_btn);

        // on students btn click
        studentsBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                personsListIntent.putExtra(Library.INTENT_DATA_TYPE, Library.STUDENT);
                startActivity(personsListIntent);
            }
        });
        // on professors btn click
        professorsBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                personsListIntent.putExtra(Library.INTENT_DATA_TYPE,Library.PROFESSOR);
                startActivity(personsListIntent);
            }
        });
    }
}
