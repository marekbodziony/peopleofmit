package marekbodziony.peopleofmit;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import marekbodziony.peopleofmit.api.GlideApp;
import marekbodziony.peopleofmit.api.Library;
import marekbodziony.peopleofmit.model.Person;
import marekbodziony.peopleofmit.model.Professor;
import marekbodziony.peopleofmit.model.Student;

public class PersonDetailsFragment extends Fragment {

    private ImageView image;
    private ImageView favourite;
    private TextView name;
    private TextView title;
    private TextView department;
    private TextView yearsTxt;
    private TextView yearsVal;
    private ImageView genderImg;
    private TextView gender;
    private TextView dateOfBirth;
    private TextView phone;
    private TextView email;

    private Person person;
    private String type;

    public PersonDetailsFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_person_details,container,false);

        image = (ImageView) view.findViewById(R.id.fragment_details_image);
        favourite = (ImageView) view.findViewById(R.id.fragment_details_favourite);
        name = (TextView) view.findViewById(R.id.fragment_details_name);
        title = (TextView) view.findViewById(R.id.fragment_details_title);
        department = (TextView) view.findViewById(R.id.fragment_details_department);
        yearsTxt = (TextView) view.findViewById(R.id.fragment_details_year_text);
        yearsVal = (TextView) view.findViewById(R.id.fragment_details_year_value);
        genderImg = (ImageView) view.findViewById(R.id.fragment_details_gender_img);
        gender = (TextView) view.findViewById(R.id.fragment_details_gender_value);
        dateOfBirth = (TextView) view.findViewById(R.id.fragment_details_dateofbirth);
        phone = (TextView) view.findViewById(R.id.fragment_details_phone);
        email = (TextView) view.findViewById(R.id.fragment_details_email);

        showPersonDetails(person);

        return view;
    }

    // set person to display
    public void setPerson(Person person){ this.person = person;}


    // display Person details
    private void showPersonDetails(Person p){
        type = person instanceof Professor ? Library.PROFESSOR : Library.STUDENT;
        name.setText(p.getName());
        department.setText(p.getDepartment());
        gender.setText(p.getGender());
        if (p.getGender().equals("female")) genderImg.setImageResource(R.drawable.female);
        dateOfBirth.setText(p.getDateOfBirth());
        phone.setText(p.getPhone());
        email.setText(p.getEmail());
        GlideApp.with(getContext()).load(p.getImageURL()).into(image);
        if (type.equals(Library.PROFESSOR)){
            title.setText(", " + ((Professor)p).getTitle());
            yearsVal.setText("" + ((Professor)p).getYearsOfWork());
        }else{
            title.setVisibility(View.INVISIBLE);
            yearsTxt.setText("Year of study:");
            yearsVal.setText(""+((Student)p).getYearOfStudy());
        }
    }
}
